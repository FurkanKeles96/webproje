﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ödevv.Startup))]
namespace ödevv
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
